import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Publish App",
        debugShowCheckedModeBanner: false,
        initialRoute: "My route",
        routes: {},
        theme: ThemeData(primaryColor: Colors.indigo));
  }
}
